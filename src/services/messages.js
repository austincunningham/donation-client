/**
 * Created by austin on 22/11/2016.
 */
export class TotalUpdate {
  constructor(total) {
    this.total = total;
  }
}

export class LoginStatus {
  constructor(status) {
    this.status = status;
  }
}
