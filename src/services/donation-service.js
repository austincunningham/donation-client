/**
 * Created by austin on 16/11/2016.
 */
import {inject} from 'aurelia-framework';
import Fixtures from './fixtures';
import {TotalUpdate, LoginStatus } from './messages';
import {EventAggregator} from 'aurelia-event-aggregator';
import AsyncHttpClient from './async-http-client';

@inject(Fixtures, EventAggregator, AsyncHttpClient)

export default class DonationService {


  donations = [];
  methods = [];
  candidates = [];
  users = [];
  total = 0;

  constructor(data, ea, ac) {
    this.methods = data.methods;
    this.ea = ea;
    this.ac = ac;
    this.getCandidates();
    this.getUsers();
  }

  getCandidates() {
    this.ac.get('/api/candidates').then(res => {
      this.candidates = res.content;
    });
  }

  getUsers() {
    this.ac.get('/api/users').then(res => {
      this.users = res.content;
    });
  }

  donate(amount, method, candidate) {
    let donation = {
      amount: amount,
      method: method
      //candidate: candidate
    };
    this.ac.post('/api/candidates/' + candidate._id + '/donations', donation).then(res => {
      const returnedDonation = res.content;
      this.donations.push(returnedDonation);
      console.log(amount + ' donated to ' + candidate.firstName + ' ' + candidate.lastName + ': ' + method);

      this.total = this.total + parseInt(amount, 10);
      console.log('Total so far ' + this.total);
      this.ea.publish(new TotalUpdate(this.total));
    });
  }

  newCandidate(firstName, lastName, office) {
    let cand = {
      firstName: firstName,
      lastName: lastName,
      office: office
    };
    this.candidates.push(cand);
    console.log(firstName, lastName, office);
  }

  login(email, password) {
    const status = {
      success: false,
      message: 'Login Attempt Failed'
    };
    for (let i = 0; i < this.users.length; i++) {
      if ( email === this.users[i].email && password === this.users[i].password) {
        console.log('authenticate ' + this.users[i].email)
        status.success = true;
        status.message = 'logged in';
        //return status;
        this.ea.publish(new LoginStatus(status));
      }
    }
    return false;
  }

  register(firstName, lastName, email, password) {
    const newUser = {
      firstName: firstName,
      lastName: lastName,
      email: email,
      password: password
    };
    this.users[email] = newUser;
  }

  logout() {
    const status = {
      success: false,
      message: ''
    };
    this.ea.publish(new LoginStatus(new LoginStatus(status)));
  }
}

