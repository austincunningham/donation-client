/**
 * Created by austin on 25/11/2016.
 */
import DonationService from '../../services/donation-service';
import {inject} from 'aurelia-framework';

@inject(DonationService)
export class Logout {

  constructor(donationService) {
    this.donationService = donationService;
  }

  logout() {
    console.log('logging out');
    this.donationService.logout();
  }
}
