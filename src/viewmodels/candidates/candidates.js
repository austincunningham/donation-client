/**
 * Created by austin on 16/11/2016.
 */
import {inject} from 'aurelia-framework';
import DonationService from '../../services/donation-service';

@inject(DonationService)
export class Candidate {

  constructor(ds) {
    this.donationServices = ds;
  }

  addCandidate() {
    console.log(this.firstName, this.lastName, this.office);
    this.donationServices.newCandidate(this.firstName, this.lastName, this.office);
  }
}
