/**
 * Created by austin on 25/11/2016.
 */
import {inject} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import DonationService from '../../services/donation-service';
import {LoginStatus} from '../../services/messages';

@inject( DonationService)
export class Login {

  email = 'marge@simpson.com';
  password = 'secret';

  constructor(ds) {
    //this.ea = ea;
    this.donationService = ds;
    this.prompt = '';
  }

  login(e) {
    console.log(`Trying to log in ${this.email}`);
    this.donationService.login(this.email, this.password);
    //this.ea.publish(new LoginStatus(status));
  }
}
